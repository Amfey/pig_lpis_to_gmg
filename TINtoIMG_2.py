'''******************************************************************
Skrypt przetwarzajacy dane TIN na TIF dostepne dla GM
Autor: Andrzej Michalski
******************************************************************'''
# Import system modules
import arcpy
from arcpy import env
import exceptions, sys, traceback

try:
    arcpy.CheckOutExtension("3D")
    # Set environment setting
    env.workspace = "D:\LPIS\M-33tif_test"
    # Set Local Variables
    dataType = "FLOAT"
    method = "LINEAR"
    sampling = "CELLSIZE 1"
    zfactor = "1"
    # Create list of TINs
    TINList = arcpy.ListDatasets("*", "Tin")
    # Verify the presence of TINs in the list
    if TINList:
        # Iterate through the list of TINs
        for dataset in TINList:
            # Define the name of the output file
            outRaster = "{0}.img".format(dataset)
            # Execute TinRaster
            arcpy.ddd.TinRaster(dataset, outRaster, dataType, 
                                method, sampling, zfactor)
        print "Finished."
    else:
        print "There are no TIN(s) in {0}.".format(env.workspace)
    arcpy.CheckInExtension("3D")
except arcpy.ExecuteError:
    print arcpy.GetMessages()
except:
    # Get the traceback object
    tb = sys.exc_info()[2]
    tbinfo = traceback.format_tb(tb)[0]
    # Concatenate error information into message string
    pymsg = 'PYTHON ERRORS:\nTraceback info:\n{0}\nError Info:\n{1}'\
          .format(tbinfo, str(sys.exc_info()[1]))
    msgs = 'ArcPy ERRORS:\n {0}\n'.format(arcpy.GetMessages(2))
    # Return python error messages for script tool or Python Window
    arcpy.AddError(pymsg)
    arcpy.AddError(msgs)
